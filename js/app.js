$(function(){
$("#show_pwd").click(function(){
    $(this).toggleClass("Open");
    $(this).children().toggleClass("far fa-eye-slash");
    if($(this).hasClass("Open")){
       $("#pwd").attr("type","text");
    }
    else
    {
        $("#pwd").attr("type","password");
    }
});
$("#show_pwd").mouseover(function(){
    if($(this).children().hasClass("far fa-eye-slash")){
       $(this).attr("title","Ẩn mật khẩu");
    }
    else
    {
        $(this).attr("title","Hiện mật khẩu");
    }
});
$(".form_submit").click(function()
{
    $("#thongbao").text("");
    if($("#user").val()=="")
    {
        $("#thongbao").text("Nhập tên đăng nhập!");
        $("#user").select().focus();
        return false;
    }
    if($("#user").val().trim()=="")
    {
        $("#thongbao").text("Tên đăng nhập không đúng!");
        $("#user").select().focus();
        return false;
    }
    if($("#pwd").val()=="")
    {
        $("#thongbao").text("Nhập mật khẩu!");
        $("#pwd").select().focus();
        return false;
    }
    if($("#user").val()!="admin")
    {
        $("#thongbao").text("Tên đăng nhập không đúng!");
        $("#user").select().focus();
        return false;
    }
    if($("#pwd").val().trim()==""||$("#pwd").val()!="admin")
    {
        $("#thongbao").text("Mật khẩu không đúng!");
        $("#pwd").select().focus();
        return false;
    }
    
});
    // $( function() {
    //     $( document).tooltip();
    // } );
});